## Glints React Native Batch 10


<details>
<summary>BioData</summary>

<p>Buat sebuah function yang mereturn Biodata lengkap dalam bentuk object Example : </p>

```javascript
	{
		name: 'Audy Wisuda Pratama',
		age: 18,
		address: '-',
		hobbies: ['coding', 'music', 'Basket', 'Fingerstyle'],
		is_married: false,
		list_school: [
			{
				name: '-',
				year_in: '-',
				year_out: '-',
				major: false / true,
			},
		],
		skills: [
			{
				skill_name: 'HTML',
				level: 'advanced',
			},
			{
				skill_name: 'CSS',
				level: 'advanced',
			},
			{
				skill_name: 'Javascript',
				level: 'beginner',
			},
			{
				skill_name: 'Java',
				level: 'beginner',
			},
		],
		interest_in_coding: true,
	}
```

</details>  

<details>
<summary>Flow Chart</summary>
<ul>
<li>Buat Flow chart sederhana, Tiket reservation di bioskop</li>
</ul>
</details>  

<details>
<summary>Login Screen</summary>
<ul>
<li>Buat Login Screen, lengkap dengan form validation (React-Native) tampilan bebas silahkan cari di dribbble atau di source lain</li>
</ul>
</details>

<details>
<summary>Penghitung Character</summary>
<ul>
<li>Buatlah Sebuah function dengan 2 paramater, dengan parameter kedua digunakan sebagai acuan untuk menghitung banyak nya huruf vokal pada suatu kata di paramater 1, contoh : </li>
<li>hitungChar(Peeenggaaris , e) tampilkan 3</li>
<li>hitungChar(Peeenggaaris , a) tampilkan 2</li>
</ul>
</details>

<details>
<summary>Git Intro</summary>
<ul>

<li> Staging, Commit, push , pull , and Conflict </li>

</ul>
</details>

<details>
<summary>Data Structure</summary>
<ul>

<li> Bermain game melalui kahot dan codeWars </li>

</ul>
</details>

<details>
<summary>React Intro</summary>
<ul>

<li> Membuat web sederhana menggunakan HTML dan React </li>

</ul>
</details>

<details>
<summary>React Native Intro</summary>
<ul>

<li> Membuat aplikasi sederhana menggunakan React native </li>

</ul>
</details>

<details>
<summary>Latihan Function</summary>
<ul>

<li>
Create and Implementation Functionact </li>

</ul>
</details>




